
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//Создать метод getAge() который будет возвращать сколько пользователю лет.
//Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
// соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser(name, surname, birthday) {

    let newUser = {
        userName: name,
        userLastName: surname,
        birthDate : birthday,
        getLogin(){
            return (this.userName[0].toUpperCase() + this.userLastName.toLowerCase())
        },
        getAge() {
            let ageArray = this.birthDate.split('.')
            let newAgeArray = new Date(Number(ageArray[2]), Number(ageArray[1]), Number(ageArray[0]))
            let todayDate = new Date()
            let todayDateNew = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate())
            let nowYearDay = new Date(todayDateNew.getFullYear(), newAgeArray.getMonth(), newAgeArray.getDate())
            let age = todayDateNew.getFullYear() - newAgeArray.getFullYear()
            if (todayDateNew < nowYearDay) {
                age = age - 1;
            }
            return age
        },
        getPassword(){
            let passYear = this.birthDate.split('.')
            let password = `${this.userName[0].toUpperCase()}${this.userLastName.toLowerCase()}${passYear[2]}`
            return password
        }
    }
    return newUser
}

let question = createNewUser(
    prompt('What is your name'),
    prompt('What is your last name'),
    prompt('Enter your birthday dd.mm.yyyy')
)

// console.log(question.getLogin())
// console.log(question.getAge())
// console.log(question.getPassword())





